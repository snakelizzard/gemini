#include "application.h"

int main(int argc, char* argv[])
{
  Application a(argc, argv);
  a.run();
  return 0;
}
