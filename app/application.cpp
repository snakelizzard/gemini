#include "application.h"

#include <QDebug>

#include "src/main_appobject.h"
#include "src/main_window.h"

Application::Application(int& argc, char** argv)
  : QtSingleApplication(argc, argv)
  , mcontext(nullptr)
{
  setApplicationName("gemini");
  setApplicationDisplayName("Gemini");
  setApplicationVersion("0.1.0");

  QIcon icon;
  icon.addFile(":/gemini-16.png", QSize(16, 16));
  icon.addFile(":/gemini-24.png", QSize(24, 24));
  icon.addFile(":/gemini-48.png", QSize(48, 48));
  setWindowIcon(icon);

  connect(this, SIGNAL(aboutToQuit()), this, SLOT(cleanup()));
  connect(this, SIGNAL(messageReceived(QString)), this, SLOT(activate()));
}

Application::~Application()
{
  cleanup();
}

void Application::run()
{
  try
  {
    if (isRunning())
    {
      sendMessage("");
#ifndef QT_DEBUG
      return;
#endif
    }

    mcontext = AppObject::create([this]() { return mcontext; }, this);

    if (!mcontext)
      return;

    mcontext->initialize();
    mcontext->window()->show();
    exec();
    cleanup();
  }
  catch (std::exception& ex)
  {
    qCritical().noquote() << "Unexpected:" << QString::fromLocal8Bit(ex.what());
  }
  catch (...)
  {
    qCritical().noquote() << "Unexpected: unknwon error";
  }
}

void Application::cleanup()
{
  if (mcontext)
    mcontext->cleanup();
}

void Application::activate()
{
#ifdef QT_DEBUG
  quit();
#else
  MainWindow* window = mcontext->window();
  window->setWindowState(window->windowState() & ~Qt::WindowMinimized);
  window->raise();
  window->activateWindow();
#endif
}
