#pragma once

#include "qtsingleapplication.h"

class AppObject;

class Application : public QtSingleApplication
{
  Q_OBJECT

public:
  Application(int& argc, char** argv);
  ~Application();

  void run();

private:
  AppObject* mcontext;

private Q_SLOTS:
  void cleanup();
  void activate();
};
