#!/bin/sh

mkdir -p build/conan

conan install conanfile.txt \
  --build missing \
  --install-folder build/conan \
  || exit 1
 
