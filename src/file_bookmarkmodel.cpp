#include "file_bookmarkmodel.h"

#include <QIcon>

#include "main_file.h"
#include "util_list.h"

BookmarkModel::BookmarkModel(List<SetBookmark>* data, QObject* parent)
  : Model<SetBookmark>(data, parent)
{
}

QVariant BookmarkModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid() || index.model() != this)
    return QVariant();

  switch (role)
  {
  case Qt::EditRole:
    return item(index)->name();
  case Qt::DisplayRole:
    [[fallthrough]];
  case Qt::ToolTipRole:
    return description(index);
  case Qt::DecorationRole:
    return File::icon(item(index)->location());
  }

  return QVariant();
}

QString BookmarkModel::description(const QModelIndex& index) const
{
  SetBookmark* it = item(index);
  return QStringLiteral("%1\n%2").arg(it->name()).arg(it->location());
}
