#pragma once

#include <QDateTime>
#include <QFileInfo>

class File : public QObject
{
  Q_OBJECT

public:
  Q_INVOKABLE File();
  explicit File(const QFileInfo& info);
  explicit File(const QString& path);

  bool operator==(const File& other) const;
  bool operator!=(const File& other) const;

  bool isDir() const;
  bool isFile() const;
  bool isRoot() const;
  QString fileName() const;
  QString name() const;
  QString extension() const;
  QString path() const;
  void setPath(const QString& value);
  void setPath(const File& value);
  QString dirName() const;
  qint64 size() const;
  QDateTime stamp() const;
  QFile::Permissions permissions() const;
  QIcon icon() const;

  static QIcon icon(const QString& path);

private:
  QFileInfo mdata;
};
