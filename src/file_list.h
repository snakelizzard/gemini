#pragma once

#include <QTableView>

class FileModel;
class SetHistory;

class FileList : public QTableView
{
  Q_OBJECT

public:
  explicit FileList(QWidget* parent);

  FileModel* model() const;
  SetHistory* history() const;
  QString currentDirectory() const;
  void setCurrentDirectory(const QString& value);

Q_SIGNALS:
  void focused();
  void specialKey(QKeyEvent* event);

protected:
  void focusInEvent(QFocusEvent* event) override;
  void keyPressEvent(QKeyEvent* event) override;
  bool focusNextPrevChild(bool next) override;

private:
  FileModel* mmodel;
  SetHistory* mhistory;

  void cdUp();
  void goToRow(const QModelIndex& index);

private Q_SLOTS:
  void enter(const QModelIndex& hint = QModelIndex());
  void directoryLoaded();
};
