#pragma once

#include <QWidget>

class SetBookmark;
class QListView;
template<typename T>
class List;

class Bookmarks : public QWidget
{
  Q_OBJECT

public:
  Bookmarks(List<SetBookmark>* data, QWidget* parent);

private:
  QListView* mlist;

private Q_SLOTS:
  void add();
  void remove();
  void up();
  void down();
};
