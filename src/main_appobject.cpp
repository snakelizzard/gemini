#include "main_appobject.h"

#include <QDebug>

#include "file_tab.h"
#include "main_file.h"
#include "main_pool.h"
#include "main_settings.h"
#include "main_window.h"
#include "settings_bookmark.h"
#include "view_tab.h"

static std::function<AppObject*()> getter;

AppObject::AppObject(QObject* parent)
  : QObject(parent)
  , msettings(new Settings(this))
  , mpool(new Pool(this))
  , mwindow(nullptr)
{
  Q_INIT_RESOURCE(res);

  qRegisterMetaType<File*>();
  qRegisterMetaType<FileTab*>();
  qRegisterMetaType<SetBookmark*>();
  qRegisterMetaType<ViewTab*>();

  mwindow = new MainWindow();
}

AppObject::~AppObject()
{
  cleanup();
}

AppObject* AppObject::create(const std::function<AppObject*()>& globalGetter, QObject* parent)
{
  if (getter)
  {
    qWarning() << "AppContext already created";
    return nullptr;
  }

  assert(parent);
  getter = globalGetter;
  return new AppObject(parent);
}

AppObject* AppObject::get()
{
  assert(getter);
  return getter();
}

Settings* AppObject::settings() const
{
  return msettings;
}

MainWindow* AppObject::window() const
{
  return mwindow;
}

Pool* AppObject::pool() const
{
  return mpool;
}

void AppObject::initialize()
{
  if (!msettings->load(mwindow))
  {
    delete mwindow;
    mwindow = new MainWindow();
    mwindow->initializeDefaults();
  }

  mwindow->initialize();
}

void AppObject::cleanup()
{
  if (mpool)
  {
    delete mpool;
    mpool = nullptr;
  }

  if (msettings && mwindow)
    msettings->save(mwindow);

  if (mwindow)
  {
    delete mwindow;
    mwindow = nullptr;
  }

  if (msettings)
  {
    delete msettings;
    msettings = nullptr;
  }
}
