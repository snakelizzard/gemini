#pragma once

#include <QValidator>
#include <QWidget>

class FileList;
class FilterValidator;
class QLineEdit;

class Finder : public QValidator
{
public:
  explicit Finder(QObject* parent);

  virtual bool selectNext(const QString& text) const = 0;
  virtual bool selectPrev(const QString& text) const = 0;
};

class FindBar : public QWidget
{
  Q_OBJECT

public:
  explicit FindBar(QWidget* parent);

  void addLetter(const QString& value);
  void setFinder(Finder* value);

public Q_SLOTS:
  void selectNext();
  void selectPrev();

Q_SIGNALS:
  void specialKey(QKeyEvent* event);

protected:
  void hideEvent(QHideEvent* event) override;
  bool eventFilter(QObject* watched, QEvent* event) override;

private:
  QLineEdit* medit;
  Finder* mfinder;
};
