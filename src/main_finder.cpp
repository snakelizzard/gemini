#include "main_finder.h"

#include <QBoxLayout>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>
#include <QToolButton>

#include "util.h"

Finder::Finder(QObject* parent)
  : QValidator(parent)
{
}

FindBar::FindBar(QWidget* parent)
  : QWidget(parent, Qt::Popup)
  , medit(new QLineEdit(this))
  , mfinder(nullptr)
{
  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setContentsMargins(4, 4, 4, 4);
  layout->setSpacing(4);
  layout->addWidget(new QLabel(tr("Search:"), this));
  layout->addWidget(medit);
  layout->addWidget(Util::button(":/down.png", this, SLOT(selectNext())));
  layout->addWidget(Util::button(":/up.png", this, SLOT(selectPrev())));
  layout->addWidget(Util::button(":/close.png", this, SLOT(close())));

  Util::setMonospaceFont(medit);
  medit->setFocus(Qt::OtherFocusReason);
  medit->installEventFilter(this);
}

void FindBar::addLetter(const QString& value)
{
  if (value.isEmpty())
    return;

  medit->setText(medit->text() + value);
}

void FindBar::setFinder(Finder* value)
{
  mfinder = value;
  medit->clear();
  medit->setValidator(value);
}

void FindBar::selectNext()
{
  if (mfinder)
    mfinder->selectNext(medit->text());
}

void FindBar::selectPrev()
{
  if (mfinder)
    mfinder->selectPrev(medit->text());
}

void FindBar::hideEvent(QHideEvent* event)
{
  QWidget::hideEvent(event);

  if (event->isAccepted())
  {
    delete mfinder;
    mfinder = nullptr;
  }
}

bool FindBar::eventFilter(QObject* watched, QEvent* event)
{
  if (watched != medit || event->type() != QEvent::KeyPress || !mfinder)
    return false;

  QKeyEvent* ke = static_cast<QKeyEvent*>(event);

  switch (ke->key())
  {
  case Qt::Key_Up:
  {
    if (mfinder->selectPrev(medit->text()))
      return true;
  }
  break;

  case Qt::Key_Down:
  {
    if (mfinder->selectNext(medit->text()))
      return true;
  }
  break;

  case Qt::Key_Tab:
    if (!ke->modifiers())
    {
      hide();
      specialKey(ke);
      return true;
    }
    break;
  }

  return false;
}
