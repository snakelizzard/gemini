#include "main_file.h"

#include <QDir>
#include <QFileIconProvider>
#include <QFileInfo>

static QFileIconProvider provider;

File::File()
  : mdata()
{
}

File::File(const QFileInfo& info)
  : mdata(info)
{
}

File::File(const QString& path)
  : mdata(path)
{
}

bool File::operator==(const File& other) const
{
  return mdata == other.mdata;
}

bool File::operator!=(const File& other) const
{
  return mdata != other.mdata;
}

bool File::isDir() const
{
  return mdata.isDir();
}

bool File::isFile() const
{
  return mdata.isFile();
}

bool File::isRoot() const
{
  return mdata.isRoot();
}

QString File::fileName() const
{
  return mdata.fileName();
}

QString File::name() const
{
  return mdata.baseName();
}

QString File::extension() const
{
  return mdata.completeSuffix();
}

QString File::path() const
{
  return mdata.absoluteFilePath();
}

void File::setPath(const QString& value)
{
  mdata.setFile(value);
}

void File::setPath(const File& value)
{
  mdata = value.mdata;
}

QString File::dirName() const
{
  return isRoot() ? "/" : mdata.dir().dirName();
}

qint64 File::size() const
{
  return mdata.size();
}

QDateTime File::stamp() const
{
  return mdata.lastModified();
}

QFileDevice::Permissions File::permissions() const
{
  return mdata.permissions();
}

QIcon File::icon() const
{
  return provider.icon(mdata);
}

QIcon File::icon(const QString& path)
{
  return provider.icon(path);
}
