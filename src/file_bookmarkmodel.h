#pragma once

#include <QAbstractItemModel>

#include "settings_bookmark.h"
#include "util_model.h"

template<typename T>
class List;

class BookmarkModel : public Model<SetBookmark>
{
  Q_OBJECT

public:
  BookmarkModel(List<SetBookmark>* data, QObject* parent);

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

private:
  QString description(const QModelIndex& index) const;
};
