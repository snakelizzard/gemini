#include "main_tab.h"

#include <QJsonObject>

#include "file_tab.h"
#include "main_appobject.h"

Tab::Tab(QWidget* parent)
  : QWidget(parent)
{
}

void Tab::setActive(bool value)
{
  Q_UNUSED(value)
}

bool Tab::isActive() const
{
  return false;
}

bool Tab::needCollapse() const
{
  return false;
}

Finder* Tab::createFind() const
{
  return nullptr;
}

const QMetaObject* Tab::contextMeta() const
{
  return nullptr;
}

void Tab::fromJson(const QJsonObject& jso)
{
  setLocation(jso["location"].toString());
}

QJsonObject Tab::toJson() const
{
  QJsonObject jso;
  jso["location"] = location();
  return jso;
}

bool Tab::isFile() const
{
  return metaObject() == &FileTab::staticMetaObject;
}

FileTab* Tab::toFile()
{
  return isFile() ? static_cast<FileTab*>(this) : nullptr;
}

const FileTab* Tab::toFile() const
{
  return isFile() ? static_cast<const FileTab*>(this) : nullptr;
}

MainWindow* Tab::window() const
{
  return AppObject::get()->window();
}
