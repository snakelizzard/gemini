#include "main_window.h"

#include <QAction>
#include <QApplication>
#include <QDir>
#include <QGridLayout>
#include <QJsonArray>
#include <QJsonObject>
#include <QKeyEvent>
#include <QMenuBar>
#include <QScreen>
#include <QStyle>

#include "file_list.h"
#include "file_model.h"
#include "file_tab.h"
#include "main_ctxapp.h"
#include "main_ctxpanel.h"
#include "main_execute.h"
#include "main_finder.h"
#include "main_panel.h"
#include "settings_bookmark.h"

MainWindow::MainWindow()
  : mleft(new Panel(this))
  , mright(new Panel(this))
  , mactive(nullptr)
  , mhint(nullptr)
  , mfind(new FindBar(this))
  , mexecute(new FileExecute(this))
  , mapp(new AppContext(this))
  , mcontext(nullptr)
  , mpanel(new PanelContext(this))
  , mlasttab(nullptr)
  , mmenu(new QMenuBar(this))
  , mbookmarks(new List<SetBookmark>(this))
  , mpanel_hint(-1)
  , mcollapsed(false)
{
  QWidget* central = new QWidget(this);
  QGridLayout* gridLayout = new QGridLayout(central);
  gridLayout->setHorizontalSpacing(4);
  gridLayout->setVerticalSpacing(0);
  gridLayout->setContentsMargins(0, 0, 0, 0);
  gridLayout->addWidget(mleft, 0, 0, 1, 1);
  gridLayout->addWidget(mright, 0, 1, 1, 1);
  gridLayout->addWidget(mexecute, 1, 0, 1, 2);

  setMenuBar(mmenu);
  setCentralWidget(central);

  // tabs
  connect(mleft, SIGNAL(tabBarDoubleClicked(int)), this, SLOT(closeTab()));
  connect(mright, SIGNAL(tabBarDoubleClicked(int)), this, SLOT(closeTab()));
  connect(mleft, SIGNAL(currentChanged(int)), this, SLOT(activatePanel()));
  connect(mright, SIGNAL(currentChanged(int)), this, SLOT(activatePanel()));
  connect(mleft, SIGNAL(focused()), this, SLOT(activatePanel()));
  connect(mright, SIGNAL(focused()), this, SLOT(activatePanel()));
  connect(mleft, SIGNAL(selected()), this, SLOT(updateActions()));
  connect(mright, SIGNAL(selected()), this, SLOT(updateActions()));
  connect(mleft, SIGNAL(specialKey(QKeyEvent*)), this, SLOT(specialKey(QKeyEvent*)));
  connect(mright, SIGNAL(specialKey(QKeyEvent*)), this, SLOT(specialKey(QKeyEvent*)));
  connect(mleft, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(tabContextMenu(QPoint)));
  connect(mright, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(tabContextMenu(QPoint)));

  // widgets
  connect(mfind, SIGNAL(specialKey(QKeyEvent*)), this, SLOT(specialKey(QKeyEvent*)));
  connect(mexecute, SIGNAL(specialKey(QKeyEvent*)), this, SLOT(specialKey(QKeyEvent*)));
}

void MainWindow::initializeDefaults()
{
  setRect(QString());
}

void MainWindow::initialize()
{
  if (!mleft->count())
    newFileTab(mleft);
  if (!mright->count())
    newFileTab(mright);
  if (!mactive)
  {
    mactive = mleft;
    activatePanel();
  }
}

Panel* MainWindow::sourcePanel(Panel* hint) const
{
  if (hint)
    return hint;

  if (mhint)
    return mhint;

  return mactive;
}

Panel* MainWindow::targetPanel(Panel* hint) const
{
  if (!hint)
    hint = sourcePanel();
  return hint == mleft ? mright : mleft;
}

FileExecute* MainWindow::execute() const
{
  return mexecute;
}

FindBar* MainWindow::find() const
{
  return mfind;
}

bool MainWindow::collapsed()
{
  return !targetPanel()->isVisible();
}

void MainWindow::setCollapsed(bool value)
{
  if (collapsed() == value)
    return;

  mcollapsed = value;
  targetPanel()->setVisible(!value);
  mpanel->updateActions();
}

void MainWindow::showFind(Panel* hint, const QString& letter)
{
  Tab* tab = hint->activeTab();

  if (!tab)
    return;

  if (!mfind->isVisible())
  {
    Finder* impl = tab->createFind();

    if (!impl)
      return;

    QRect rect = QStyle::alignedRect(
        Qt::LayoutDirectionAuto,
        Qt::AlignBottom | Qt::AlignLeft,
        mfind->sizeHint(),
        QRect(mapToGlobal(hint->geometry().topLeft()), hint->geometry().size()));

    rect.translate(0, mfind->sizeHint().height() + 1);
    mfind->setGeometry(rect);
    mfind->setFinder(impl);
    mfind->show();
  }

  mfind->addLetter(letter);
}

List<SetBookmark>* MainWindow::bookmarks() const
{
  return mbookmarks;
}

void MainWindow::fromJson(const QJsonObject& jso)
{
  mbookmarks->clear();

  for (const QJsonValue& it : jso["bookmarks"].toArray())
    if (it.isObject())
      mbookmarks->append()->fromJson(it.toObject());

  setRect(jso["rect"].toString());
  activatePanel(jso["index"].toInt() ? mright : mleft);
  mleft->fromJson(jso["left"].toObject());
  mright->fromJson(jso["right"].toObject());
}

QJsonObject MainWindow::toJson() const
{
  QJsonArray bookmarks;

  for (const SetBookmark* it : *mbookmarks)
    bookmarks.append(it->toJson());

  QJsonObject jso;
  jso["rect"] = rect();
  jso["index"] = sourcePanel() == mleft ? 0 : 1;
  jso["left"] = mleft->toJson();
  jso["right"] = mright->toJson();
  jso["bookmarks"] = bookmarks;
  return jso;
}

FileTab* MainWindow::newFileTab(Panel* hint, QString location)
{
  Panel* source = sourcePanel(hint);
  FileTab* tab = new FileTab(source);
  Tab* active = source->activeTab();

  if (!location.isEmpty())
    tab->setLocation(location);
  else if (active)
    tab->setLocation(active->location());
  else
    tab->setLocation(QDir::homePath());

  source->addTab(tab);
  return tab;
}

void MainWindow::closeTab(Panel* hint)
{
  sourcePanel(hint)->closeTab(mpanel_hint);
}

void MainWindow::activatePanel(Panel* hint)
{
  if (!hint)
    hint = qobject_cast<Panel*>(sender());

  if (!hint)
    hint = mactive;
  else
    mactive = hint;

  if (hint->activeTab() == mlasttab)
    return;

  mlasttab = hint->activeTab();
  mright->setActive(hint == mright);
  mleft->setActive(hint == mleft);

  if (sourcePanel()->activeTab()->needCollapse())
    targetPanel()->setVisible(false);
  else
    targetPanel()->setVisible(!mcollapsed);

  activateContext();
  mpanel->updateActions();
}

QString MainWindow::rect() const
{
  QRect r = geometry();
  return QString::asprintf("@Rect(%d %d %d %d)", r.x(), r.y(), r.width(), r.height());
}

void MainWindow::setRect(const QString& value)
{
  QRect rect;

  if (value.startsWith("@Rect(") && value.endsWith(")"))
  {
    auto refs = value.midRef(6, value.size() - 7).split(' ');

    if (refs.size() == 4)
      rect = QRect(refs[0].toInt(), refs[1].toInt(), refs[2].toInt(), refs[3].toInt());
  }

  if (rect.isEmpty())
  {
    rect = QStyle::alignedRect(Qt::LayoutDirectionAuto,
                               Qt::AlignCenter,
                               QSize(1280, 768),
                               QApplication::screens().first()->availableGeometry());
  }

  setGeometry(rect);
}

void MainWindow::assignPanel(Panel* from, Panel* to)
{
  FileTab* tab = from->activeTab()->toFile();

  if (!tab)
    return;

  FileModel* source = tab->model();
  QModelIndex idx = tab->list()->currentIndex();

  if (idx.isValid() && source->item(idx)->isDir())
    to->activeTab()->setLocation(source->item(idx)->path());
  else
    to->activeTab()->setLocation(tab->location());
}

void MainWindow::activateContext()
{
  const QMetaObject* ctxmeta = mcontext ? mcontext->metaObject() : nullptr;
  const QMetaObject* srcmeta = sourcePanel()->activeTab()->contextMeta();

  if (ctxmeta == srcmeta)
    return;

  if (mcontext)
    delete mcontext;

  assert(srcmeta->inherits(&Context::staticMetaObject));
  mcontext = static_cast<Context*>(srcmeta->newInstance(Q_ARG(QObject*, this)));

  for (QAction* it : actions())
    removeAction(it);

  mmenu->clear();
  mapp->tuneMenu(this, mmenu);

  if (mcontext)
    mcontext->tuneMenu(this, mmenu);

  mpanel->tuneMenu(this, mmenu);
}

void MainWindow::specialKey(QKeyEvent* event)
{
  if (event->isAutoRepeat())
    return;

  Panel* hint = qobject_cast<Panel*>(sender());

  if (!hint)
    hint = sourcePanel();

  switch (event->key())
  {
  case Qt::Key_Tab:
    if (!event->modifiers() && !collapsed())
      activatePanel(targetPanel(hint));
    break;
  case Qt::Key_Left:
    if (event->modifiers() == Qt::ControlModifier && hint == mright)
      assignPanel(mright, mleft);
    else if (!event->modifiers())
      mexecute->activate();
    break;
  case Qt::Key_Right:
    if (event->modifiers() == Qt::ControlModifier && hint == mleft)
      assignPanel(mleft, mright);
    else if (!event->modifiers())
      mexecute->activate();
    break;
  case Qt::Key_Escape:
    if (!event->modifiers())
    {
      if (!hint->activeTab()->isFile() && hint->activeTab()->isActive())
        closeTab(hint);
      else if (hint->activeTab()->isActive() && collapsed())
        setCollapsed(false);
      else
        activatePanel(hint);
    }
    break;
  case Qt::Key_Up:
    [[fallthrough]];
  case Qt::Key_Down:
    if (!event->modifiers())
      activatePanel(hint);
    break;
  }

  if ((event->modifiers() == Qt::AltModifier ||
       event->modifiers() == (Qt::AltModifier | Qt::ShiftModifier)) &&
      ((event->key() >= Qt::Key_0 && event->key() <= Qt::Key_9) ||
       (event->key() >= Qt::Key_A && event->key() <= Qt::Key_Z)))
  {
    showFind(hint, event->text());
  }
}

void MainWindow::tabContextMenu(const QPoint& point)
{
  Panel* hint = qobject_cast<Panel*>(sender());

  if (!hint)
    return;

  mhint = hint;
  mpanel_hint = hint->tabBar()->tabAt(point);
  mpanel->updateActions();
  mpanel->menu()->exec(hint->mapToGlobal(point));
  mhint = nullptr;
  mpanel_hint = -1;
  mpanel->updateActions();
}

void MainWindow::updateActions()
{
  if (mcontext)
    mcontext->updateActions();
}
