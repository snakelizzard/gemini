#include "main_settings.h"

#include <QDebug>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStandardPaths>

#include "main_window.h"

Settings::Settings(QObject* parent)
  : QObject(parent)
{
}

bool Settings::load(MainWindow* target)
{
  QString fn = QStandardPaths::locate(QStandardPaths::AppConfigLocation, fileName());

  if (fn.isEmpty())
    return false;

  QFile f(fn);

  if (!f.open(QIODevice::ReadOnly))
    return false;

  QJsonDocument doc = QJsonDocument::fromJson(f.readAll());

  if (!doc.isObject())
    return false;

  target->fromJson(doc.object());
  return true;
}

void Settings::save(MainWindow* target) const
{
  QString d = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
  if (!QDir().mkpath(d))
    return;

  QFile f(d + "/" + fileName());

  if (!f.open(QIODevice::WriteOnly))
    return;

  QJsonDocument doc(target->toJson());
  f.write(doc.toJson());
}

QString Settings::fileName() const
{
  return
#ifdef QT_DEBUG
      "config-debug.json"
#else
      "config.json"
#endif
      ;
}
