#pragma once

#include <QDialog>

class BookmarkDialog : public QDialog
{
  Q_OBJECT

public:
  explicit BookmarkDialog(QWidget* parent);
};
