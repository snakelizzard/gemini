#pragma once

#include <QObject>

class Settings;
class MainWindow;
class Pool;

class AppObject : public QObject
{
  Q_OBJECT

public:
  ~AppObject();

  static AppObject* create(const std::function<AppObject*()>& globalGetter, QObject* parent);
  static AppObject* get();

  Settings* settings() const;
  MainWindow* window() const;
  Pool* pool() const;

  void initialize();
  void cleanup();

private:
  Settings* msettings;
  Pool* mpool;
  MainWindow* mwindow;

  AppObject(QObject* parent);
};
