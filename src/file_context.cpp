#include "file_context.h"

#include <QAction>

#include "file_tab.h"
#include "main_execute.h"
#include "main_file.h"
#include "main_panel.h"
#include "main_window.h"
#include "view_tab.h"

FileContext::FileContext(QObject* parent)
  : Context(tr("File"), parent)
{
  addShortcut("Ctrl+Return", SLOT(fileToExecute()));
  addShortcut("Ctrl+P", SLOT(pathToExecute()));
  addShortcut("Shift+F6", SLOT(rename()));
  addShortcut("Ctrl+D", SLOT(showBookmarks()));

  mview = addAction(tr("View"), SLOT(view()), "F3");
}

void FileContext::updateActions()
{
  FileTab* tab = activeFileTab();
  File* file = tab ? tab->currentFile() : nullptr;
  mview->setEnabled(file && file->isFile());
}

FileTab* FileContext::activeFileTab() const
{
  return activeTab()->toFile();
}

void FileContext::fileToExecute()
{
  FileTab* tab = activeFileTab();

  if (tab)
    window()->execute()->putFile(tab->currentFile());
}

void FileContext::pathToExecute()
{
  FileTab* tab = activeFileTab();

  if (tab)
    window()->execute()->putPath(tab->currentFile());
}

void FileContext::rename()
{
  FileTab* tab = activeFileTab();

  if (tab)
    tab->edit();
}

void FileContext::view()
{
  FileTab* tab = activeFileTab();

  if (!tab)
    return;

  File* info = tab->currentFile();

  if (!info || !info->isFile())
    return;

  Panel* panel = window()->sourcePanel();
  ViewTab* view = new ViewTab(panel);
  view->setLocation(info->path());
  panel->addTab(view);
}

void FileContext::showBookmarks()
{
  FileTab* tab = activeFileTab();

  if (tab)
    tab->showBookmarks();
}
