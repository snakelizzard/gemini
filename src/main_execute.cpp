#include "main_execute.h"

#include <QBoxLayout>
#include <QComboBox>
#include <QDir>
#include <QKeyEvent>
#include <QLabel>
#include <QLineEdit>

#include "main_file.h"
#include "util.h"

FileExecute::FileExecute(QWidget* parent)
  : QWidget(parent)
  , mcombo(new QComboBox(this))
{
  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setContentsMargins(4, 4, 4, 4);
  layout->addWidget(new QLabel(tr("Execute:"), this));
  layout->addWidget(mcombo);
  Util::setMonospaceFont(mcombo);

  mcombo->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
  mcombo->setEditable(true);
  mcombo->installEventFilter(this);
}

void FileExecute::putFile(const File* info)
{
  if (!info)
    return;

  QString text = mcombo->currentText();
  QString toAdd = info->fileName();

  if (info->isDir())
    toAdd += QDir::separator();

  if (text.endsWith('\"'))
  {
    text.chop(1);
    toAdd += '\"';

    if (!Util::pathEndsWithSepartor(text))
      text += QDir::separator();
  }
  else if (!Util::pathEndsWithSepartor(text) && !text.isEmpty())
  {
    if (toAdd.contains(' '))
      toAdd = '\"' + toAdd + '\"';

    toAdd = ' ' + toAdd;
  }
  else if (toAdd.contains(' '))
  {
    int idx = text.lastIndexOf(' ');
    toAdd += '\"';

    if (idx == -1)
      text.prepend('\"');
    else
      text.insert(idx + 1, '\"');
  }

  mcombo->setCurrentText(text + toAdd);
}

void FileExecute::putPath(const File* info)
{
  if (!info)
    return;

  QString text = mcombo->currentText();
  QString toAdd = info->path() + QDir::separator();

  if (toAdd.contains(' '))
    toAdd = '\"' + toAdd + '\"';

  if (!text.isEmpty())
    toAdd = ' ' + toAdd;

  mcombo->setCurrentText(text + toAdd);
}

void FileExecute::activate()
{
  mcombo->lineEdit()->selectAll();
  mcombo->setFocus(Qt::MouseFocusReason);
}

bool FileExecute::eventFilter(QObject* watched, QEvent* event)
{
  if (watched != mcombo || event->type() != QEvent::KeyPress)
    return false;

  QKeyEvent* ke = static_cast<QKeyEvent*>(event);

  switch (ke->key())
  {
  case Qt::Key_Escape:
    [[fallthrough]];
  case Qt::Key_Up:
    [[fallthrough]];
  case Qt::Key_Down:
    [[fallthrough]];
  case Qt::Key_Tab:
    if (!ke->modifiers())
    {
      specialKey(ke);
      return true;
    }
    break;
  }

  return false;
}
