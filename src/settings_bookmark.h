#pragma once

#include <QObject>

class SetBookmark : public QObject
{
  Q_OBJECT

public:
  explicit Q_INVOKABLE SetBookmark() = default;

  const QString& name() const;
  void setName(const QString& value);
  const QString& location() const;
  void setLocation(const QString& value);

  void fromJson(const QJsonObject& jso);
  QJsonObject toJson() const;

private:
  QString mname;
  QString mlocation;
};
