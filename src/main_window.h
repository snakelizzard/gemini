#pragma once

#include <QMainWindow>

#include "util_list.h"

class Context;
class FileExecute;
class FileTab;
class FindBar;
class Panel;
class SetBookmark;
class Tab;
class QMenuBar;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow();

  void initializeDefaults();
  void initialize();
  Panel* sourcePanel(Panel* hint = nullptr) const;
  Panel* targetPanel(Panel* hint = nullptr) const;
  FileExecute* execute() const;
  FindBar* find() const;
  bool collapsed();
  void setCollapsed(bool value);
  void showFind(Panel* hint, const QString& letter);
  List<SetBookmark>* bookmarks() const;

  void fromJson(const QJsonObject& jso);
  QJsonObject toJson() const;

public Q_SLOTS:
  FileTab* newFileTab(Panel* hint = nullptr, QString location = QString());
  void closeTab(Panel* hint = nullptr);
  void activatePanel(Panel* hint = nullptr);

private:
  Panel* mleft;
  Panel* mright;
  Panel* mactive;
  Panel* mhint;
  FindBar* mfind;
  FileExecute* mexecute;
  Context* mapp;
  Context* mcontext;
  Context* mpanel;
  Tab* mlasttab;
  QMenuBar* mmenu;
  List<SetBookmark>* mbookmarks;
  int mpanel_hint;
  bool mcollapsed;

  QString rect() const;
  void setRect(const QString& value);
  void assignPanel(Panel* from, Panel* to);
  void activateContext();

private Q_SLOTS:
  void specialKey(QKeyEvent* event);
  void tabContextMenu(const QPoint& point);
  void updateActions();
};
