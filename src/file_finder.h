#pragma once

#include "main_finder.h"

class FileList;

class FileFinder : public Finder
{
public:
  explicit FileFinder(FileList* target);

  State validate(QString& text, int& pos) const override;
  bool selectNext(const QString& text) const override;
  bool selectPrev(const QString& text) const override;

private:
  FileList* mtarget;

  bool selectNext(const QString& text, int pos) const;
  bool selectPrev(const QString& text, int pos) const;
  QModelIndex selected() const;
};
