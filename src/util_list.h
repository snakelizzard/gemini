#pragma once

#include <QObject>
#include <QScopedPointer>

class ListData
{
public:
  explicit ListData(const QMetaObject* meta);
  ~ListData();

  void clear();

  const QMetaObject* mmeta;
  QList<QObject*> mlist;
};

class ListBase : public QObject
{
  Q_OBJECT

public:
  ListBase(const QMetaObject* meta, QObject* parent = nullptr);

  bool setTypeName(const QString& value);
  QList<QObject*>::const_iterator begin() const;
  QList<QObject*>::iterator begin();
  QList<QObject*>::const_iterator end() const;
  QList<QObject*>::iterator end();
  int size() const;
  int indexOf(QObject* value) const;
  bool isEmpty() const;
  QObject* append();
  QObject* at(int index) const;
  void steal(ListBase& other);
  void clear();

protected:
  QScopedPointer<ListData> mdata;

  void append(QObject* value);
};

template<typename T>
class List : public ListBase
{
public:
  explicit List(QObject* parent = nullptr)
      : ListBase(&T::staticMetaObject, parent)
  {
  }

  class const_iterator
  {
  public:
    QList<QObject*>::const_iterator mit;
    typedef std::random_access_iterator_tag iterator_category;
    typedef qptrdiff difference_type;
    typedef T* value_type;
    typedef const T** pointer;
    typedef const T*& reference;

    inline const_iterator(QList<QObject*>::const_iterator it) noexcept
        : mit(it)
    {
    }
    inline reference operator*() const
    {
      return *reinterpret_cast<pointer>(mit.operator->());
    }
    inline pointer operator->() const
    {
      return static_cast<pointer>(mit.operator->());
    }
    inline reference operator[](difference_type j) const
    {
      return static_cast<reference>(mit[j]);
    }
    inline bool operator==(const const_iterator& o) const noexcept
    {
      return mit == o.mit;
    }
    inline bool operator!=(const const_iterator& o) const noexcept
    {
      return mit != o.mit;
    }
    inline const_iterator& operator++()
    {
      ++mit;
      return *this;
    }
    inline const_iterator operator++(int)
    {
      return mit++;
    }
    inline const_iterator& operator--()
    {
      --mit;
      return *this;
    }
    inline const_iterator operator--(int)
    {
      return mit--;
    }
    inline const_iterator& operator+=(difference_type j)
    {
      mit += j;
      return *this;
    }
    inline const_iterator& operator-=(difference_type j)
    {
      mit -= j;
      return *this;
    }
    inline const_iterator operator+(difference_type j) const
    {
      return const_iterator(mit + j);
    }
    inline const_iterator operator-(difference_type j) const
    {
      return const_iterator(mit - j);
    }
    inline int operator-(const const_iterator& j) const
    {
      return mit - j.mit;
    }
    inline bool operator<(const const_iterator& other) const noexcept
    {
      return mit < other.mit;
    }
    inline bool operator<=(const const_iterator& other) const noexcept
    {
      return mit <= other.mit;
    }
    inline bool operator>(const const_iterator& other) const noexcept
    {
      return mit > other.mit;
    }
    inline bool operator>=(const const_iterator& other) const noexcept
    {
      return mit >= other.mit;
    }
  };

  class iterator
  {
  public:
    QList<QObject*>::iterator mit;
    typedef std::random_access_iterator_tag iterator_category;
    typedef qptrdiff difference_type;
    typedef T* value_type;
    typedef T** pointer;
    typedef T*& reference;

    inline iterator(QList<QObject*>::iterator it) noexcept
        : mit(it)
    {
    }
    inline reference operator*() const
    {
      return *reinterpret_cast<pointer>(mit.operator->());
    }
    inline pointer operator->() const
    {
      return static_cast<pointer>(mit.operator->());
    }
    inline reference operator[](difference_type j) const
    {
      return static_cast<reference>(mit[j]);
    }
    inline bool operator==(const iterator& o) const noexcept
    {
      return mit == o.mit;
    }
    inline bool operator!=(const iterator& o) const noexcept
    {
      return mit != o.mit;
    }
    inline iterator& operator++()
    {
      ++mit;
      return *this;
    }
    inline iterator operator++(int)
    {
      return mit++;
    }
    inline iterator& operator--()
    {
      --mit;
      return *this;
    }
    inline iterator operator--(int)
    {
      return mit--;
    }
    inline iterator& operator+=(difference_type j)
    {
      mit += j;
      return *this;
    }
    inline iterator& operator-=(difference_type j)
    {
      mit -= j;
      return *this;
    }
    inline iterator operator+(difference_type j) const
    {
      return iterator(mit + j);
    }
    inline iterator operator-(difference_type j) const
    {
      return iterator(mit - j);
    }
    inline int operator-(const iterator& j) const
    {
      return mit - j.mit;
    }
    inline bool operator<(const iterator& other) const noexcept
    {
      return mit < other.mit;
    }
    inline bool operator<=(const iterator& other) const noexcept
    {
      return mit <= other.mit;
    }
    inline bool operator>(const iterator& other) const noexcept
    {
      return mit > other.mit;
    }
    inline bool operator>=(const iterator& other) const noexcept
    {
      return mit >= other.mit;
    }
  };

  const_iterator begin() const
  {
    return const_iterator(ListBase::begin());
  }
  iterator begin()
  {
    return iterator(ListBase::begin());
  }
  const_iterator end() const
  {
    return const_iterator(ListBase::end());
  }
  iterator end()
  {
    return iterator(ListBase::end());
  }
  T* append()
  {
    return static_cast<T*>(ListBase::append());
  }
  T* operator[](int index) const
  {
    return static_cast<T*>(mdata->mlist[index]);
  }
  void append(T* value)
  {
    ListBase::append(value);
  }
  T* front() const
  {
    return static_cast<T*>(mdata->mlist.front());
  }
  T* back() const
  {
    return static_cast<T*>(mdata->mlist.back());
  }
  bool takeOne(T* value)
  {
    return mdata->mlist.removeOne(value);
  }
  T* takeAt(int index)
  {
    return static_cast<T*>(mdata->mlist.takeAt(index));
  }
};
