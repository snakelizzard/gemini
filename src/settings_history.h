#pragma once

#include <QObject>

class File;
template<typename T>
class List;

class SetHistory : public QObject
{
  Q_OBJECT

public:
  explicit SetHistory(QObject* parent);

  void record(const File* value);
  const File* back() const;

  void fromJson(const QJsonArray& jso);
  QJsonArray toJson() const;

private:
  List<File>* mdata;
};
