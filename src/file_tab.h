#pragma once

#include "main_tab.h"

class File;
class FileModel;
class FileList;
class SetHistory;
class QFrame;
class QLabel;

class FileTab : public Tab
{
  Q_OBJECT

public:
  explicit Q_INVOKABLE FileTab(QWidget* parent);

  QString title() const override;
  QString location() const override;
  void setLocation(const QString& value) override;
  void setActive(bool value) override;
  bool isActive() const override;
  Finder* createFind() const override;
  const QMetaObject* contextMeta() const override;
  virtual void fromJson(const QJsonObject& jso) override;
  virtual QJsonObject toJson() const override;

  File* currentFile() const;
  FileModel* model() const;
  FileList* list() const;
  SetHistory* history() const;
  void edit();

public Q_SLOTS:
  void showBookmarks();

private:
  QFrame* mheader;
  QLabel* mstatus;
  FileList* mlist;

private Q_SLOTS:
  void listUpdated();
};
