#include "file_list.h"

#include <QDesktopServices>
#include <QDir>
#include <QHeaderView>
#include <QKeyEvent>
#include <QPalette>
#include <QTimer>

#include "file_model.h"
#include "settings_history.h"
#include "util.h"

FileList::FileList(QWidget* parent)
  : QTableView(parent)
  , mmodel(new FileModel(this))
  , mhistory(new SetHistory(this))
{
  setFocusPolicy(Qt::ClickFocus);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  setEditTriggers(QAbstractItemView::NoEditTriggers);
  setTabKeyNavigation(false);
  setDragEnabled(true);
  setDragDropMode(QAbstractItemView::DragDrop);
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setShowGrid(false);
  setWordWrap(false);
  setCornerButtonEnabled(false);
  horizontalHeader()->setMinimumSectionSize(48);
  horizontalHeader()->setDefaultSectionSize(64);
  horizontalHeader()->setHighlightSections(false);
  verticalHeader()->setVisible(false);

  QPalette pal = palette();
  pal.setColor(QPalette::Inactive,
               QPalette::Highlight,
               pal.color(QPalette::Active, QPalette::Highlight).lighter(150));
  setPalette(pal);

  Util::setMonospaceFont(this);
  mmodel->setSmallFont(font());
  setModel(mmodel);

  QHeaderView* header = horizontalHeader();
  header->setSectionResizeMode(0, QHeaderView::Stretch);
  header->setSectionResizeMode(2, QHeaderView::ResizeToContents);
  header->setSectionResizeMode(3, QHeaderView::ResizeToContents);
  header->setSectionResizeMode(4, QHeaderView::ResizeToContents);

  connect(this, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(enter(QModelIndex)));
  connect(mmodel, SIGNAL(reloaded()), this, SLOT(directoryLoaded()), Qt::QueuedConnection);
}

FileModel* FileList::model() const
{
  return mmodel;
}

SetHistory* FileList::history() const
{
  return mhistory;
}

QString FileList::currentDirectory() const
{
  return mmodel->rootPath()->path();
}

void FileList::setCurrentDirectory(const QString& value)
{
  if (value.isEmpty())
    return;

  QString cwd = currentDirectory();

  if (value == cwd)
    return;

  File f(value);

  if (!cwd.isEmpty())
    mhistory->record(mmodel->rootPath());

  mmodel->setRootPath(&f);
}

void FileList::focusInEvent(QFocusEvent* event)
{
  QTableView::focusInEvent(event);
  focused();
}

void FileList::keyPressEvent(QKeyEvent* event)
{
  switch (event->key())
  {
  case Qt::Key_Escape:
    [[fallthrough]];
  case Qt::Key_Tab:
    if (!event->modifiers())
    {
      specialKey(event);
      return;
    }
    break;
  case Qt::Key_Left:
    [[fallthrough]];
  case Qt::Key_Right:
    if (event->modifiers() == Qt::ControlModifier || !event->modifiers())
    {
      specialKey(event);
      return;
    }
    break;
  case Qt::Key_Return:
    [[fallthrough]];
  case Qt::Key_Enter:
    if (!event->modifiers())
    {
      enter();
      return;
    }
    break;
  case Qt::Key_Backspace:
    if (!event->modifiers())
    {
      cdUp();
      return;
    }
    break;
  case Qt::Key_Home:
    if (!event->modifiers())
    {
      goToRow(mmodel->index(0, 0, rootIndex()));
      return;
    }
    break;
  case Qt::Key_End:
    if (!event->modifiers())
    {
      QModelIndex root = rootIndex();
      goToRow(mmodel->index(mmodel->rowCount(root) - 1, 0, root));
      return;
    }
    break;
  }

  if ((event->modifiers() == Qt::AltModifier ||
       event->modifiers() == (Qt::AltModifier | Qt::ShiftModifier)) &&
      ((event->key() >= Qt::Key_0 && event->key() <= Qt::Key_9) ||
       (event->key() >= Qt::Key_A && event->key() <= Qt::Key_Z)))
  {
    specialKey(event);
    return;
  }

  QTableView::keyPressEvent(event);
}

bool FileList::focusNextPrevChild(bool next)
{
  Q_UNUSED(next);
  return false;
}

void FileList::cdUp()
{
  QDir root(mmodel->rootPath()->path());

  if (root.cdUp())
    setCurrentDirectory(root.path());
}

void FileList::goToRow(const QModelIndex& index)
{
  selectRow(index.row());
  scrollTo(index, PositionAtCenter);
}

void FileList::enter(const QModelIndex& hint)
{
  QModelIndex idx = hint;

  if (!idx.isValid())
    idx = currentIndex();

  if (!idx.isValid())
    return;

  File* f = mmodel->item(idx);

  if (f->isDir())
  {
    setCurrentDirectory(f->path());
    return;
  }

  QDesktopServices::openUrl(QUrl::fromLocalFile(f->path()));
}

void FileList::directoryLoaded()
{
  QModelIndex index = mmodel->search(mhistory->back());

  if (!index.isValid())
    index = mmodel->index(0, 0, rootIndex());

  goToRow(index);
}
