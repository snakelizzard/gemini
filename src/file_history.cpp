#include "file_history.h"

#include <QBoxLayout>
#include <QListView>
#include <QToolButton>

#include "file_historymodel.h"
#include "util.h"

FileHistory::FileHistory(List<File>* data, QWidget* parent)
  : QWidget(parent)
  , mlist(new QListView(this))
{
  mlist->setModel(new HistoryModel(data, this));
  setAttribute(Qt::WA_DeleteOnClose, true);

  QVBoxLayout* vertical = new QVBoxLayout(this);
  vertical->setSpacing(4);
  vertical->setContentsMargins(4, 4, 4, 4);

  QHBoxLayout* horizontal = new QHBoxLayout();
  horizontal->setSpacing(4);
  horizontal->setContentsMargins(0, 0, 0, 0);
  horizontal->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
  horizontal->addWidget(Util::button(":/close.png", this, SLOT(close())));

  vertical->addWidget(mlist);
  vertical->addItem(horizontal);
}
