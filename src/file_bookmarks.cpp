#include "file_bookmarks.h"

#include <QBoxLayout>
#include <QListView>
#include <QToolButton>

#include "file_bookmarkdlg.h"
#include "file_bookmarkmodel.h"
#include "settings_bookmark.h"
#include "util.h"

Bookmarks::Bookmarks(List<SetBookmark>* data, QWidget* parent)
  : QWidget(parent, Qt::Popup)
  , mlist(new QListView(this))
{
  mlist->setModel(new BookmarkModel(data, this));
  setAttribute(Qt::WA_DeleteOnClose, true);

  QVBoxLayout* vertical = new QVBoxLayout(this);
  vertical->setSpacing(4);
  vertical->setContentsMargins(4, 4, 4, 4);

  QHBoxLayout* horizontal = new QHBoxLayout();
  horizontal->setSpacing(4);
  horizontal->setContentsMargins(0, 0, 0, 0);
  horizontal->addWidget(Util::button(":/add.png", tr("Add"), this, SLOT(add())));
  horizontal->addWidget(Util::button(":/remove.png", tr("Remove"), this, SLOT(remove())));
  horizontal->addWidget(Util::button(":/up.png", this, SLOT(up())));
  horizontal->addWidget(Util::button(":/down.png", this, SLOT(down())));
  horizontal->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
  horizontal->addWidget(Util::button(":/close.png", this, SLOT(close())));

  vertical->addWidget(mlist);
  vertical->addItem(horizontal);
}

void Bookmarks::add()
{
  setAttribute(Qt::WA_DeleteOnClose, false);
  BookmarkDialog dlg(this);

  if (dlg.exec() == QDialog::Accepted)
  {
  }

  setAttribute(Qt::WA_DeleteOnClose, true);
  show();
}

void Bookmarks::remove()
{
}

void Bookmarks::up()
{
}

void Bookmarks::down()
{
}
