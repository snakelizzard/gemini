#pragma once

#include "main_file.h"
#include "util_model.h"

template<typename T>
class List;

class HistoryModel : public Model<File>
{
  Q_OBJECT

public:
  HistoryModel(List<File>* data, QObject* parent);

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
};
