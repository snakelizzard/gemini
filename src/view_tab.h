#pragma once

#include "main_tab.h"

class ViewBrowser;

class ViewTab : public Tab
{
  Q_OBJECT

public:
  explicit Q_INVOKABLE ViewTab(QWidget* parent);

  QString title() const override;
  QString location() const override;
  void setLocation(const QString& value) override;
  void setActive(bool value) override;
  bool isActive() const override;
  bool needCollapse() const override;
  Finder* createFind() const override;
  const QMetaObject* contextMeta() const override;

  ViewBrowser* browser() const;

private:
  ViewBrowser* mtext;
  QString mlocation;

private Q_SLOTS:
  void contentAvailable(const QVariant& data);
};
