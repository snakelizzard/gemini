#pragma once

#include "main_context.h"

class ViewContext : public Context
{
  Q_OBJECT

public:
  explicit Q_INVOKABLE ViewContext(QObject* parent);

private Q_SLOTS:
  void find();
  void findNext();
  void findPrev();
};
