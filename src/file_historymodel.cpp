#include "file_historymodel.h"

#include <QIcon>

#include "util_list.h"

HistoryModel::HistoryModel(List<File>* data, QObject* parent)
  : Model<File>(data, parent)
{
}

QVariant HistoryModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid() || index.model() != this)
    return QVariant();

  switch (role)
  {
  case Qt::DisplayRole:
    [[fallthrough]];
  case Qt::ToolTipRole:
    return item(index)->path();
  case Qt::DecorationRole:
    return item(index)->icon();
  }

  return QVariant();
}
