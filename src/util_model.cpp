#include "util_model.h"

#include "util_list.h"

ModelBase::ModelBase(ListBase* list, QObject* parent)
  : QAbstractItemModel(parent)
  , mdata(list)
{
}

QModelIndex ModelBase::index(int row, int column, const QModelIndex& parent) const
{
  return parent.isValid() ? QModelIndex() : createIndex(row, column);
}

QModelIndex ModelBase::parent(const QModelIndex& child) const
{
  Q_UNUSED(child);
  return QModelIndex();
}

int ModelBase::rowCount(const QModelIndex& parent) const
{
  return parent.isValid() ? 0 : mdata->size();
}

int ModelBase::columnCount(const QModelIndex& parent) const
{
  return parent.isValid() ? 0 : 1;
}

QObject* ModelBase::item(const QModelIndex& index) const
{
  return (index.isValid() && index.model() == this) ? mdata->at(index.row()) : nullptr;
}

void ModelBase::steal(ListBase* value)
{
  beginResetModel();
  mdata->steal(*value);
  endResetModel();
}

QModelIndex ModelBase::indexOf(QObject* value)
{
  int row = mdata->indexOf(value);
  return row >= 0 ? index(row, 0) : QModelIndex();
}
