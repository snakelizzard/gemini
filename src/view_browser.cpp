#include "view_browser.h"

#include <QKeyEvent>

#include "util.h"

ViewBrowser::ViewBrowser(QWidget* parent)
  : QTextBrowser(parent)
{
  setFocusPolicy(Qt::ClickFocus);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  setOpenLinks(false);
  setOpenExternalLinks(true);
  setTabChangesFocus(false);
  setLineWrapMode(QTextBrowser::NoWrap);
  Util::setMonospaceFont(this);
}

void ViewBrowser::focusInEvent(QFocusEvent* event)
{
  QTextBrowser::focusInEvent(event);
  focused();
}

void ViewBrowser::keyPressEvent(QKeyEvent* event)
{
  switch (event->key())
  {
  case Qt::Key_Escape:
    [[fallthrough]];
  case Qt::Key_Tab:
    if (!event->modifiers())
    {
      specialKey(event);
      return;
    }
    break;
  }

  QTextBrowser::keyPressEvent(event);
}

bool ViewBrowser::focusNextPrevChild(bool next)
{
  Q_UNUSED(next);
  return false;
}
