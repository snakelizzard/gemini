#include "view_finder.h"

#include "view_browser.h"

ViewFinder::ViewFinder(ViewBrowser* target)
  : Finder(target)
  , mtarget(target)
{
}

QValidator::State ViewFinder::validate(QString& text, int& pos) const
{
  QTextCursor cursor = mtarget->textCursor();

  if (cursor.hasSelection())
    cursor.setPosition(cursor.selectionStart());

  while (!text.isEmpty())
  {
    mtarget->setTextCursor(cursor);

    if (selectNext(text))
      break;

    text.chop(1);
  }

  pos = text.size();

  if (text.isEmpty())
    mtarget->setTextCursor(cursor);

  return QValidator::Acceptable;
}

bool ViewFinder::selectNext(const QString& text) const
{
  return mtarget->find(text, QTextDocument::FindFlags());
}

bool ViewFinder::selectPrev(const QString& text) const
{
  return mtarget->find(text, QTextDocument::FindBackward);
}
