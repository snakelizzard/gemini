#include "view_tab.h"

#include <QBoxLayout>
#include <QDebug>
#include <QFile>
#include <QTextCodec>

#include "main_pool.h"
#include "view_browser.h"
#include "view_context.h"
#include "view_finder.h"

ViewTab::ViewTab(QWidget* parent)
  : Tab(parent)
  , mtext(new ViewBrowser(this))
{
  QVBoxLayout* vertical = new QVBoxLayout(this);
  vertical->setSpacing(0);
  vertical->setContentsMargins(0, 0, 0, 0);
  vertical->addWidget(mtext);

  connect(mtext, SIGNAL(focused()), this, SIGNAL(focused()));
  connect(mtext, SIGNAL(specialKey(QKeyEvent*)), this, SIGNAL(specialKey(QKeyEvent*)));
}

QString ViewTab::title() const
{
  return QUrl::fromLocalFile(mlocation).fileName();
}

QString ViewTab::location() const
{
  return mlocation;
}

void ViewTab::setLocation(const QString& value)
{
  if (mlocation == value || value.isEmpty())
    return;

  mlocation = value;
  Pool::get()->post(
      [value](QVariant& data) {
        QVariantMap map;
        map["loc"] = value;
        QFile f(value);

        if (!f.open(QFile::ReadOnly))
        {
          qWarning() << "Can not open file:" << value << f.errorString();
          map["src"] = tr("Can not open: ") + value + '\n' + f.errorString();
          data = map;
          return;
        }

        QByteArray ba = f.readAll();
        QTextCodec* codec = QTextCodec::codecForHtml(ba);
        map["src"] = codec->toUnicode(ba);
        data = map;
      },
      this,
      SLOT(contentAvailable(QVariant)));
}

void ViewTab::setActive(bool value)
{
  if (value)
    mtext->setFocus(Qt::OtherFocusReason);
}

bool ViewTab::isActive() const
{
  return mtext->hasFocus();
}

bool ViewTab::needCollapse() const
{
  return true;
}

Finder* ViewTab::createFind() const
{
  return new ViewFinder(mtext);
}

const QMetaObject* ViewTab::contextMeta() const
{
  return &ViewContext::staticMetaObject;
}

ViewBrowser* ViewTab::browser() const
{
  return mtext;
}

void ViewTab::contentAvailable(const QVariant& data)
{
  QVariantMap map = data.toMap();
  if (mlocation == map["loc"].toString())
    mtext->setPlainText(map["src"].toString());
}
