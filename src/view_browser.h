#pragma once

#include <QTextBrowser>

class ViewBrowser : public QTextBrowser
{
  Q_OBJECT

public:
  explicit ViewBrowser(QWidget* parent);

Q_SIGNALS:
  void focused();
  void specialKey(QKeyEvent* event);

protected:
  void focusInEvent(QFocusEvent* event) override;
  void keyPressEvent(QKeyEvent* event) override;
  bool focusNextPrevChild(bool next) override;
};
