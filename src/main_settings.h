#pragma once

#include <QObject>

class MainWindow;

class Settings : public QObject
{
  Q_OBJECT

public:
  explicit Settings(QObject* parent);

  bool load(MainWindow* target);
  void save(MainWindow* target) const;

private:
  QString fileName() const;
};
