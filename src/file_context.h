#pragma once

#include "main_context.h"

class FileTab;

class FileContext : public Context
{
  Q_OBJECT

public:
  explicit Q_INVOKABLE FileContext(QObject* parent);

  void updateActions() override;

private:
  QAction* mview;

  FileTab* activeFileTab() const;

private Q_SLOTS:
  void fileToExecute();
  void pathToExecute();
  void rename();
  void view();
  void showBookmarks();
};
