#include "file_tab.h"

#include <QBoxLayout>
#include <QFrame>
#include <QJsonArray>
#include <QJsonObject>
#include <QLabel>
#include <QPalette>
#include <QStyle>
#include <QToolButton>

#include "file_bookmarks.h"
#include "file_context.h"
#include "file_finder.h"
#include "file_list.h"
#include "file_model.h"
#include "main_file.h"
#include "main_window.h"
#include "settings_history.h"
#include "util.h"

FileTab::FileTab(QWidget* parent)
  : Tab(parent)
  , mheader(new QFrame(this))
  , mstatus(new QLabel(mheader))
  , mlist(new FileList(this))
{
  QVBoxLayout* vertical = new QVBoxLayout(this);
  vertical->setSpacing(0);
  vertical->setContentsMargins(0, 0, 0, 0);

  QHBoxLayout* horizontal = new QHBoxLayout(mheader);
  horizontal->setContentsMargins(4, 4, 4, 4);
  horizontal->addWidget(mstatus);
  horizontal->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
  horizontal->addWidget(Util::button(":/bookmark.png", this, SLOT(showBookmarks())));

  vertical->addWidget(mheader);
  vertical->addWidget(mlist);

  QPalette pal = mheader->palette();
  pal.setColor(
      QPalette::Active, QPalette::Window, pal.color(QPalette::Active, QPalette::Highlight));
  mheader->setPalette(pal);

  connect(model(), SIGNAL(reloaded()), this, SLOT(listUpdated()));
  connect(list(), SIGNAL(focused()), this, SIGNAL(focused()));
  connect(list(), SIGNAL(specialKey(QKeyEvent*)), this, SIGNAL(specialKey(QKeyEvent*)));
  connect(list()->selectionModel(),
          SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
          this,
          SIGNAL(selected()));
}

QString FileTab::title() const
{
  return mlist->model()->rootPath()->dirName();
}

QString FileTab::location() const
{
  return mlist->currentDirectory();
}

File* FileTab::currentFile() const
{
  return mlist->model()->item(mlist->currentIndex());
}

FileModel* FileTab::model() const
{
  return mlist->model();
}

FileList* FileTab::list() const
{
  return mlist;
}

SetHistory* FileTab::history() const
{
  return mlist->history();
}

void FileTab::setLocation(const QString& value)
{
  if (location() != value)
    mlist->setCurrentDirectory(value);
}

void FileTab::setActive(bool value)
{
  if (value)
    mlist->setFocus(Qt::OtherFocusReason);

  mheader->setAutoFillBackground(value);
}

bool FileTab::isActive() const
{
  return mlist->hasFocus();
}

Finder* FileTab::createFind() const
{
  return new FileFinder(mlist);
}

const QMetaObject* FileTab::contextMeta() const
{
  return &FileContext::staticMetaObject;
}

void FileTab::fromJson(const QJsonObject& jso)
{
  history()->fromJson(jso["history"].toArray());
  Tab::fromJson(jso);
}

QJsonObject FileTab::toJson() const
{
  QJsonObject jso = Tab::toJson();
  jso["history"] = history()->toJson();
  return jso;
}

void FileTab::edit()
{
  QModelIndex idx = mlist->currentIndex();

  if (idx.isValid() && idx.row() != 0)
    mlist->edit(idx.siblingAtColumn(0));
}

void FileTab::showBookmarks()
{
  Bookmarks* dlg = new Bookmarks(window()->bookmarks(), this);

  QRect rect = QStyle::alignedRect(
      Qt::LayoutDirectionAuto,
      Qt::AlignTop | Qt::AlignRight,
      dlg->sizeHint(),
      QRect(mapToGlobal(mlist->geometry().topLeft()), mlist->geometry().size()));

  dlg->setGeometry(rect);
  dlg->show();
}

void FileTab::listUpdated()
{
  mstatus->setText(mlist->currentDirectory());
  locationChanged();
}
