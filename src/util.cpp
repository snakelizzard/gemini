#include "util.h"

#include <QDebug>
#include <QDir>
#include <QToolButton>
#include <QWidget>

namespace Util
{

void setMonospaceFont(QWidget* widget)
{
  QFont f = widget->font();
  f.setPointSizeF(f.pointSizeF() * 0.8);
  f.setStyleHint(QFont::Monospace);
  f.setFamily("Monospace");
  widget->setFont(f);
}

bool pathEndsWithSepartor(const QString& value)
{
  return value.endsWith(QDir::separator())
#ifdef Q_OS_WINDOWS
         || value.endsWith('/')
#endif
      ;
}

QString concatPath(const QString path, const QString& file)
{
  QString res = path;
  if (!pathEndsWithSepartor(res))
    res += QDir::separator();
  return res + file;
}

const QMetaObject* findMeta(const QString& name)
{
  int id = QMetaType::type(name.toLatin1());

  if (id == QMetaType::UnknownType)
  {
    qWarning() << "Unknown type:" << name;
    return nullptr;
  }

  QMetaType meta(id);

  if (meta.metaObject() == nullptr)
  {
    qWarning() << "Not an object:" << name;
    return nullptr;
  }

  return meta.metaObject();
}

QToolButton* button(const QString& icon, QWidget* receiver, const char* slot)
{
  return button(icon, "", receiver, slot);
}

QToolButton* button(const QString& icon, const QString& text, QWidget* receiver, const char* slot)
{
  QToolButton* res = new QToolButton(receiver);
  res->setIcon(QIcon(icon));
  res->setIconSize(QSize(16, 16));
  res->setToolTip(text);
  res->setFocusPolicy(Qt::NoFocus);
  QObject::connect(res, SIGNAL(clicked(bool)), receiver, slot);
  return res;
}

} // namespace Util
