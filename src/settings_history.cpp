#include "settings_history.h"

#include <QJsonArray>
#include <QJsonObject>

#include "main_file.h"
#include "util_list.h"

SetHistory::SetHistory(QObject* parent)
  : QObject(parent)
  , mdata(new List<File>(this))
{
}

void SetHistory::record(const File* value)
{
  auto it = std::find_if(
      mdata->begin(), mdata->end(), [value](const File* cur) { return *value == *cur; });

  File* f = *it;
  if (it != mdata->end() && mdata->takeOne(f))
    mdata->append(f);
  else
    mdata->append()->setPath(*value);

  while (mdata->size() > 20)
    delete mdata->takeAt(0);
}

const File* SetHistory::back() const
{
  return mdata->isEmpty() ? nullptr : mdata->back();
}

void SetHistory::fromJson(const QJsonArray& jso)
{
  mdata->clear();

  for (const QJsonValue& it : jso)
    if (it.isString())
      mdata->append()->setPath(it.toString());
}

QJsonArray SetHistory::toJson() const
{
  QJsonArray jso;

  for (const File* it : *mdata)
    jso.append(it->path());

  return jso;
}
