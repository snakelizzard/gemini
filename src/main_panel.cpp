#include "main_panel.h"

#include <QJsonArray>
#include <QJsonObject>

#include "file_tab.h"
#include "util.h"

Panel::Panel(QWidget* parent)
  : QTabWidget(parent)
{
  setContextMenuPolicy(Qt::CustomContextMenu);
  setElideMode(Qt::ElideRight);
  setMovable(true);
  setTabBarAutoHide(true);
}

void Panel::addTab(Tab* value)
{
  QTabWidget::addTab(value, value->title());
  setCurrentWidget(value);
  connect(value, SIGNAL(locationChanged()), this, SLOT(tabUpdated()));
  connect(value, SIGNAL(focused()), this, SIGNAL(focused()));
  connect(value, SIGNAL(specialKey(QKeyEvent*)), this, SIGNAL(specialKey(QKeyEvent*)));
  connect(value, SIGNAL(selected()), this, SIGNAL(selected()));
}

void Panel::closeTab(int index)
{
  if (!canClose())
    return;

  QWidget* tab = widget(index);

  if (!tab)
  {
    index = currentIndex();
    tab = currentWidget();
  }

  removeTab(index);
  delete tab;
}

void Panel::closeOther()
{
  int current = currentIndex();

  for (int n = count() - 1; n >= 0; --n)
    if (n != current)
      closeTab(n);
}

Tab* Panel::activeTab() const
{
  return static_cast<Tab*>(currentWidget());
}

void Panel::setActive(bool value)
{
  int idx = currentIndex();

  for (int n = 0, cnt = count(); n != cnt; ++n)
  {
    Tab* tab = static_cast<Tab*>(widget(n));

    if (tab)
      tab->setActive(value && n == idx);
  }
}

bool Panel::canClose() const
{
  if (!activeTab()->isFile())
    return count() > 1;

  int ft = 0;

  for (int n = 0, cnt = count(); n != cnt; ++n)
  {
    Tab* tab = static_cast<Tab*>(widget(n));

    if (tab->isFile() && ++ft > 1)
      return true;
  }

  return false;
}

void Panel::fromJson(const QJsonObject& jso)
{
  for (int n = count() - 1; n >= 0; --n)
    closeTab(n);

  for (const QJsonValue& it : jso["tabs"].toArray())
  {
    if (!it.isObject())
      continue;

    QJsonObject info = it.toObject();
    const QMetaObject* meta = Util::findMeta(info["class"].toString() + '*');

    if (!meta || !meta->inherits(&Tab::staticMetaObject))
      continue;

    Tab* tab = static_cast<Tab*>(meta->newInstance(Q_ARG(QWidget*, this)));

    if (!tab)
      continue;

    tab->fromJson(info);
    addTab(tab);
  }

  if (count())
    setCurrentIndex(jso["index"].toInt());
}

QJsonObject Panel::toJson() const
{
  QJsonArray tabs;

  for (int n = 0, cnt = count(); n != cnt; ++n)
  {
    Tab* tab = static_cast<Tab*>(widget(n));
    QJsonObject info = tab->toJson();
    info["class"] = tab->metaObject()->className();
    tabs.append(info);
  }

  QJsonObject jso;
  jso["index"] = currentIndex();
  jso["tabs"] = tabs;
  return jso;
}

void Panel::tabUpdated()
{
  Tab* tab = qobject_cast<Tab*>(sender());

  if (!tab)
    return;

  int idx = indexOf(tab);

  if (idx == -1)
    return;

  setTabText(idx, tab->title());
}
