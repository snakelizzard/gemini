#include "main_pool.h"

#include <QDebug>
#include <QPointer>
#include <QThreadPool>

#include "main_appobject.h"

Runnable::Runnable(const std::function<void(QVariant&)>& task)
  : mtask(task)
{
  setAutoDelete(true);
}

void Runnable::run()
{
  try
  {
    QVariant data;
    mtask(data);
    done(data);
  }
  catch (std::exception& ex)
  {
    qCritical().noquote() << "Unexpected:" << QString::fromLocal8Bit(ex.what());
  }
  catch (...)
  {
    qCritical().noquote() << "Unexpected: unknwon error";
  }
}

Pool::Pool(QObject* parent)
  : QObject(parent)
  , msystem(new QThreadPool(this))
{
}

Pool* Pool::get()
{
  return AppObject::get()->pool();
}

Runnable*
Pool::post(const std::function<void(QVariant&)>& task, QObject* receiver, const char* slot)
{
  Runnable* res = new Runnable(task);
  connect(res, SIGNAL(done(QVariant)), receiver, slot, Qt::QueuedConnection);
  msystem->start(res);
  return res;
}
