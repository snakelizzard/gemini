#pragma once

#include <QWidget>

class File;
class QListView;
template<typename T>
class List;

class FileHistory : public QWidget
{
public:
  FileHistory(List<File>* data, QWidget* parent);

private:
  QListView* mlist;
};
