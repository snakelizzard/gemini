#pragma once

#include "main_finder.h"

class ViewBrowser;

class ViewFinder : public Finder
{
public:
  ViewFinder(ViewBrowser* target);

  State validate(QString& text, int& pos) const override;
  bool selectNext(const QString& text) const override;
  bool selectPrev(const QString& text) const override;

private:
  ViewBrowser* mtarget;
};
