#include "settings_bookmark.h"

#include <QJsonObject>

const QString& SetBookmark::name() const
{
  return mname;
}

void SetBookmark::setName(const QString& value)
{
  mname = value;
}

const QString& SetBookmark::location() const
{
  return mlocation;
}

void SetBookmark::setLocation(const QString& value)
{
  mlocation = value;
}

void SetBookmark::fromJson(const QJsonObject& jso)
{
  mname = jso["name"].toString();
  mlocation = jso["location"].toString();
}

QJsonObject SetBookmark::toJson() const
{
  QJsonObject jso;
  jso["name"] = mname;
  jso["location"] = mlocation;
  return jso;
}
