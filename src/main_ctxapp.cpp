#include "main_ctxapp.h"

#include <QAction>
#include <QApplication>
#include <QMessageBox>

#include "main_window.h"

AppContext::AppContext(QObject* parent)
  : Context(QApplication::applicationDisplayName(), parent)
{
  addAction(tr("About..."), SLOT(aboutApp()))->setMenuRole(QAction::AboutRole);
  addAction(tr("Quit"), SLOT(quit()))->setMenuRole(QAction::QuitRole);
}

void AppContext::aboutApp()
{
  QMessageBox::about(window(),
                     QApplication::applicationDisplayName(),
                     tr("<h3>%1</h3><p>Twin panel file manager."
                        "<br/>Copyright 2020 Paul Kolomiets."
                        "<br/>Icons by <a href='https://icons8.com'>icons8.com</a></p>"
                        "<p><table><tr><td>%2</td><td>%3</td></tr>"
                        "<tr><td>qt</td><td>%4</td></tr>"
                        "<tr><td>yaml-cpp</td><td>0.6.3</td></tr></table></p>"
                        "<p>The program is provided AS IS with NO WARRANTY OF ANY KIND, "
                        "INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS "
                        "FOR A PARTICULAR PURPOSE.</p>")
                         .arg(QApplication::applicationDisplayName())
                         .arg(QApplication::applicationName())
                         .arg(QApplication::applicationVersion())
                         .arg(QT_VERSION_STR));
}

void AppContext::quit()
{
  qApp->closeAllWindows();
}
