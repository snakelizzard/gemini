#pragma once

#include <QTabWidget>

class Tab;

class Panel : public QTabWidget
{
  Q_OBJECT

public:
  explicit Panel(QWidget* parent);

  void addTab(Tab* value);
  void closeTab(int index);
  void closeOther();
  Tab* activeTab() const;
  void setActive(bool value);
  bool canClose() const;

  void fromJson(const QJsonObject& jso);
  QJsonObject toJson() const;

Q_SIGNALS:
  void focused();
  void selected();
  void specialKey(QKeyEvent* event);

private Q_SLOTS:
  void tabUpdated();
};
