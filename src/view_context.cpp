#include "view_context.h"

#include "main_finder.h"
#include "main_panel.h"
#include "main_window.h"
#include "view_browser.h"
#include "view_tab.h"

ViewContext::ViewContext(QObject* parent)
  : Context(tr("Edit"), parent)
{
  addAction(tr("Find..."), SLOT(find()), "Ctrl+F");
  addAction(tr("Find next"), SLOT(findNext()), "F3");
  addAction(tr("Find previous"), SLOT(findPrev()), "Shift+F3");
}

void ViewContext::find()
{
  MainWindow* win = window();
  ViewTab* tab = qobject_cast<ViewTab*>(win->sourcePanel()->activeTab());

  if (!tab)
    return;

  win->showFind(win->sourcePanel(), tab->browser()->textCursor().selectedText());
}

void ViewContext::findNext()
{
  FindBar* f = window()->find();

  if (!f->isVisible())
    find();
  else
    f->selectNext();
}

void ViewContext::findPrev()
{
  FindBar* f = window()->find();

  if (!f->isVisible())
    find();
  else
    f->selectPrev();
}
