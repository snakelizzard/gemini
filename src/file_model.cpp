#include "file_model.h"

#include <cmath>

#include <QDirIterator>
#include <QIcon>

#include "main_pool.h"
#include "util_list.h"

FileModel::FileModel(QObject* parent)
  : Model<File>(new List<File>(), parent)
{
  mdata->setParent(this);
}

const File* FileModel::rootPath() const
{
  return &mroot;
}

void FileModel::setRootPath(const File* path)
{
  if (mroot == *path)
    return;

  mroot.setPath(*path);
  refresh();
}

void FileModel::refresh()
{
  QSharedPointer<File> f(new File(mroot.path()));
  Pool::get()->post([f](QVariant& data) { data = refresh(*f); }, this, SLOT(done(QVariant)));
}

QString FileModel::fileName(const QModelIndex& index) const
{
  File* info = item(index);

  if (info->isDir() || info->fileName().startsWith('.'))
    return info->fileName();

  return info->name();
}

QString FileModel::fileFullName(const QModelIndex& index) const
{
  return item(index)->fileName();
}

void FileModel::setSmallFont(const QFont& value)
{
  msmall = value;
  msmall.setPointSizeF(msmall.pointSizeF() * 0.8);
}

QModelIndex FileModel::search(const File* value) const
{
  if (value)
    for (int n = 0, cnt = mdata->size(); n != cnt; ++n)
    {
      if (*(static_cast<File*>(mdata->at(n))) == *value)
        return index(n, 0);
    }

  return QModelIndex();
}

int FileModel::columnCount(const QModelIndex& parent) const
{
  return parent.isValid() ? 0 : 5;
}

QVariant FileModel::data(const QModelIndex& index, int role) const
{
  if (!index.isValid() || index.model() != this)
    return QVariant();

  switch (role)
  {
  case Qt::EditRole:
    [[fallthrough]];
  case Qt::DisplayRole:
    switch (index.column())
    {
    case 0:
      return fileName(index);
    case 1:
      return fileExt(index);
    case 2:
      return fileSize(index);
    case 3:
      return fileDate(index);
    case 4:
      return fileMode(index);
    }
    break;
  case Qt::ToolTipRole:
    switch (index.column())
    {
    case 0:
      [[fallthrough]];
    case 1:
      return fileFullName(index);
    case 2:
      return fileSize(index);
    case 3:
      return fileDate(index);
    case 4:
      return fileMode(index);
    }
    break;
  case Qt::TextAlignmentRole:
    return alignment(index);
  case Qt::FontRole:
    return font(index);
  case Qt::DecorationRole:
    if (index.column() == 0)
      return item(index)->icon();
    break;
  }

  return QVariant();
}

bool FileModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
  Q_UNUSED(index);
  Q_UNUSED(value);
  Q_UNUSED(role);
  return false;
}

QVariant FileModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation != Qt::Horizontal || role != Qt::DisplayRole)
    return QAbstractItemModel::headerData(section, orientation, role);

  switch (section)
  {
  case 0:
    return tr("Name");
  case 1:
    return tr("Ext");
  case 2:
    return tr("Size");
  case 3:
    return tr("Date");
  case 4:
    return tr("Mode");
  }

  return QVariant();
}

QString FileModel::fileExt(const QModelIndex& index) const
{
  if (!index.row())
    return QString();

  File* info = item(index);

  if (info->isDir() || info->fileName().startsWith('.'))
    return QString();

  return info->extension();
}

QString FileModel::fileSize(const QModelIndex& index) const
{
  File* info = item(index);

  if (info->isDir())
    return tr("<DIR>");

  quint64 value = qAbs(info->size());
  int power = value ? (63 - qCountLeadingZeroBits(value)) / 10 : 0;
  QString res = mlocale.toString(value / std::pow(1024.0, power), 'f', 2);

  switch (power)
  {
  case 0:
    res += 'B';
    break;
  case 1:
    res += 'K';
    break;
  case 2:
    res += 'M';
    break;
  case 3:
    res += 'G';
    break;
  default:
    res += 'E' + QString::number(power);
    break;
  }

  return res;
}

QString FileModel::fileDate(const QModelIndex& index) const
{
  return mlocale.toString(item(index)->stamp(), QLocale::NarrowFormat);
}

QString FileModel::fileMode(const QModelIndex& index) const
{
  auto value = item(index)->permissions();
  QString res;
  res.reserve(9);

  res.append(value.testFlag(QFileDevice::ReadOwner) ? 'r' : '-');
  res.append(value.testFlag(QFileDevice::WriteOwner) ? 'w' : '-');
  res.append(value.testFlag(QFileDevice::ExeOwner) ? 'x' : '-');

  res.append(value.testFlag(QFileDevice::ReadGroup) ? 'r' : '-');
  res.append(value.testFlag(QFileDevice::WriteGroup) ? 'w' : '-');
  res.append(value.testFlag(QFileDevice::ExeGroup) ? 'x' : '-');

  res.append(value.testFlag(QFileDevice::ReadOther) ? 'r' : '-');
  res.append(value.testFlag(QFileDevice::WriteOther) ? 'w' : '-');
  res.append(value.testFlag(QFileDevice::ExeOther) ? 'x' : '-');

  return res;
}

int FileModel::alignment(const QModelIndex& index) const
{
  if (index.column() == 1 || index.column() == 2)
    return Qt::AlignRight | Qt::AlignVCenter;

  return Qt::AlignLeft | Qt::AlignVCenter;
}

QVariant FileModel::font(const QModelIndex& index) const
{
  return index.column() ? msmall : QVariant();
}

QVariant FileModel::refresh(const File& location)
{
  QSharedPointer<List<File>> list(new List<File>());
  QDirIterator it(location.path(),
                  location.isRoot()
                      ? QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Hidden
                      : QDir::AllEntries | QDir::NoDot | QDir::AllDirs | QDir::Hidden);

  while (it.hasNext())
    list->append(new File(it.next()));

  std::sort(list->begin(), list->end(), &compare);
  return QVariant::fromValue<>(list);
}

bool FileModel::compare(const File* o1, const File* o2)
{
  bool d1 = o1->isDir();
  bool d2 = o2->isDir();

  if (d1 != d2)
    return d1;

  QString n1 = o1->fileName();
  QString n2 = o2->fileName();

  if (!d1)
    return n1.compare(n2, Qt::CaseInsensitive) < 0;

  static QString dot = "..";
  bool dot1 = n1 == dot;
  bool dot2 = n2 == dot;

  if (dot1 != dot2)
    return dot1;

  return n1.compare(n2, Qt::CaseInsensitive) < 0;
  ;
}

void FileModel::done(const QVariant& data)
{
  auto ls = data.value<QSharedPointer<List<File>>>();
  steal(ls.get());
  reloaded();
}
