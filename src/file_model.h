#pragma once

#include <QFont>
#include <QLocale>

#include "main_file.h"
#include "util_model.h"

class FileModel : public Model<File>
{
  Q_OBJECT

public:
  FileModel(QObject* parent);

  const File* rootPath() const;
  void setRootPath(const File* path);
  void refresh();
  QString fileName(const QModelIndex& index) const;
  QString fileFullName(const QModelIndex& index) const;
  void setSmallFont(const QFont& value);
  QModelIndex search(const File* value) const;

  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  QVariant
  headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

Q_SIGNALS:
  void reloaded();

private:
  File mroot;
  QLocale mlocale;
  QFont msmall;

  QString fileExt(const QModelIndex& index) const;
  QString fileSize(const QModelIndex& index) const;
  QString fileDate(const QModelIndex& index) const;
  QString fileMode(const QModelIndex& index) const;
  int alignment(const QModelIndex& index) const;
  QVariant font(const QModelIndex& index) const;

  static QVariant refresh(const File& location);
  static bool compare(const File* o1, const File* o2);

private Q_SLOTS:
  void done(const QVariant& data);
};
