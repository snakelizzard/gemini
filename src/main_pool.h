#pragma once

#include <QObject>
#include <QRunnable>

class QThreadPool;

class Runnable : public QObject, public QRunnable
{
  Q_OBJECT

public:
  Runnable(const std::function<void(QVariant&)>& task);

  void run() override;

Q_SIGNALS:
  void done(const QVariant& data);

private:
  std::function<void(QVariant&)> mtask;
};

class Pool : public QObject
{
  Q_OBJECT

public:
  explicit Pool(QObject* parent);

  static Pool* get();

  Runnable* post(const std::function<void(QVariant&)>& task, QObject* receiver, const char* slot);

private:
  QThreadPool* msystem;
};
