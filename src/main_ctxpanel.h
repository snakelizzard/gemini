#pragma once

#include "main_context.h"

class PanelContext : public Context
{
  Q_OBJECT

public:
  explicit PanelContext(QObject* parent);

  void updateActions() override;

private:
  QAction* mcloseTab;
  QAction* mcloseOtherTab;
  QAction* mcollapse;

private Q_SLOTS:
  void newTab();
  void closeTab();
  void syncTabs();
  void swapTabs();
  void closeOtherTabs();
  void copyTabToTraget();
  void goRoot();
  void goHome();
  void collapse();
};
