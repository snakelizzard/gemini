#pragma once

#include <QWidget>

class FileTab;
class Finder;
class MainWindow;

class Tab : public QWidget
{
  Q_OBJECT

public:
  explicit Tab(QWidget* parent);

  virtual QString title() const = 0;
  virtual QString location() const = 0;
  virtual void setLocation(const QString& value) = 0;
  virtual void setActive(bool value);
  virtual bool isActive() const;
  virtual bool needCollapse() const;
  virtual Finder* createFind() const;
  virtual const QMetaObject* contextMeta() const;
  virtual void fromJson(const QJsonObject& jso);
  virtual QJsonObject toJson() const;

  bool isFile() const;
  FileTab* toFile();
  const FileTab* toFile() const;

Q_SIGNALS:
  void focused();
  void selected();
  void locationChanged();
  void specialKey(QKeyEvent* event);

protected:
  MainWindow* window() const;
};
