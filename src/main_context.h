#pragma once

#include <QObject>

class MainWindow;
class Tab;
class QAction;
class QMenu;
class QMenuBar;

class Context : public QObject
{
  Q_OBJECT

public:
  Context(const QString& title, QObject* parent);
  ~Context();

  void tuneMenu(QWidget* target, QMenuBar* menu) const;
  QMenu* menu() const;

public Q_SLOTS:
  virtual void updateActions();

protected:
  QMenu* mmenu;
  QList<QAction*> minvisible;

  QAction*
  addAction(const QString& name, const char* slot, const QString& keySequesnce = QString());
  QAction* addShortcut(const QString& keySequesnce, const char* slot);
  void addSeparator();
  MainWindow* window() const;
  Tab* activeTab() const;
};
