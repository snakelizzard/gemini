#pragma once

#include <QAbstractItemModel>

class ListBase;

class ModelBase : public QAbstractItemModel
{
  Q_OBJECT

public:
  ModelBase(ListBase* list, QObject* parent);

  QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex& child) const override;
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;

protected:
  ListBase* mdata;

  QObject* item(const QModelIndex& index) const;
  void steal(ListBase* value);
  QModelIndex indexOf(QObject* value);
};

template<typename T>
class Model : public ModelBase
{
public:
  Model(ListBase* list, QObject* parent)
      : ModelBase(list, parent)
  {
  }

  T* item(const QModelIndex& index) const
  {
    return static_cast<T*>(ModelBase::item(index));
  }
  QModelIndex indexOf(T* value)
  {
    return ModelBase::indexOf(value);
  }
};
