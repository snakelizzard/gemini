#include "util_list.h"

#include "util.h"

ListData::ListData(const QMetaObject* meta)
  : mmeta(meta)
{
}

ListData::~ListData()
{
  clear();
}

void ListData::clear()
{
  qDeleteAll(mlist);
  mlist.clear();
}

ListBase::ListBase(const QMetaObject* meta, QObject* parent)
  : QObject(parent)
  , mdata(new ListData(meta))
{
}

bool ListBase::setTypeName(const QString& value)
{
  const QMetaObject* meta = Util::findMeta(value);

  if (!meta)
    return false;

  mdata->mmeta = meta;
  return true;
}

QList<QObject*>::const_iterator ListBase::begin() const
{
  return mdata->mlist.begin();
}

QList<QObject*>::iterator ListBase::begin()
{
  return mdata->mlist.begin();
}

QList<QObject*>::const_iterator ListBase::end() const
{
  return mdata->mlist.end();
}

QList<QObject*>::iterator ListBase::end()
{
  return mdata->mlist.end();
}

int ListBase::size() const
{
  return mdata->mlist.size();
}

int ListBase::indexOf(QObject* value) const
{
  return mdata->mlist.indexOf(value);
}

bool ListBase::isEmpty() const
{
  return mdata->mlist.isEmpty();
}

QObject* ListBase::append()
{
  assert(mdata->mmeta);
  QObject* res = mdata->mmeta->newInstance();
  assert(res);
  mdata->mlist.append(res);
  return res;
}

QObject* ListBase::at(int index) const
{
  return mdata->mlist.at(index);
}

void ListBase::steal(ListBase& other)
{
  mdata->clear();
  mdata->mlist = other.mdata->mlist;
  mdata->mmeta = other.mdata->mmeta;
  other.mdata->mlist.clear();
}

void ListBase::clear()
{
  mdata->clear();
}

void ListBase::append(QObject* value)
{
  mdata->mlist.append(value);
}
