#pragma once

#include "main_context.h"

class AppContext : public Context
{
  Q_OBJECT

public:
  explicit AppContext(QObject* parent);

private Q_SLOTS:
  void aboutApp();
  void quit();
};
