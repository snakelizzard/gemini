#pragma once

#include <QWidget>

class File;
class QComboBox;

class FileExecute : public QWidget
{
  Q_OBJECT

public:
  explicit FileExecute(QWidget* parent);

  void putFile(const File* info);
  void putPath(const File* info);
  void activate();

Q_SIGNALS:
  void specialKey(QKeyEvent* event);

protected:
  bool eventFilter(QObject* watched, QEvent* event) override;

private:
  QComboBox* mcombo;
};
