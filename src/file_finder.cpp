#include "file_finder.h"

#include "file_list.h"
#include "file_model.h"

FileFinder::FileFinder(FileList* target)
  : Finder(target)
  , mtarget(target)
{
}

QValidator::State FileFinder::validate(QString& text, int& pos) const
{
  if (!mtarget)
    return QValidator::Invalid;

  QModelIndex current = mtarget->currentIndex();

  if (current.isValid() && selectNext(text, current.row()))
    return QValidator::Acceptable;

  while (!text.isEmpty() && !selectNext(text, 0))
    text.chop(1);

  pos = text.size();
  return QValidator::Acceptable;
}

bool FileFinder::selectNext(const QString& text) const
{
  QModelIndex idx = selected();
  return idx.isValid() && selectNext(text, idx.row() + 1);
}

bool FileFinder::selectPrev(const QString& text) const
{
  QModelIndex idx = selected();
  return idx.isValid() && selectPrev(text, idx.row() - 1);
}

bool FileFinder::selectNext(const QString& text, int pos) const
{
  QModelIndex root = mtarget->rootIndex();
  FileModel* model = mtarget->model();

  for (int n = pos, cnt = model->rowCount(root); n < cnt; ++n)
  {
    QString fn = model->fileName(model->index(n, 0, root));

    if (fn.startsWith(text, Qt::CaseInsensitive))
    {
      mtarget->selectRow(n);
      return true;
    }
  }

  return false;
}

bool FileFinder::selectPrev(const QString& text, int pos) const
{
  QModelIndex root = mtarget->rootIndex();
  FileModel* model = mtarget->model();

  for (int n = pos; n >= 0; --n)
  {
    QString fn = model->fileName(model->index(n, 0, root));

    if (fn.startsWith(text, Qt::CaseInsensitive))
    {
      mtarget->selectRow(n);
      return true;
    }
  }

  return false;
}

QModelIndex FileFinder::selected() const
{
  return mtarget ? mtarget->currentIndex() : QModelIndex();
}
