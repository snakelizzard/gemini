#include "main_context.h"

#include <QAction>
#include <QMenu>
#include <QMenuBar>

#include "main_appobject.h"
#include "main_panel.h"
#include "main_window.h"

Context::Context(const QString& title, QObject* parent)
  : QObject(parent)
  , mmenu(new QMenu(title))
{
}

Context::~Context()
{
  delete mmenu;
}

void Context::tuneMenu(QWidget* target, QMenuBar* menu) const
{
  menu->addMenu(mmenu);

  for (QAction* it : minvisible)
    target->addAction(it);
}

QMenu* Context::menu() const
{
  return mmenu;
}

void Context::updateActions()
{
  // nothing
}

QAction* Context::addAction(const QString& name, const char* slot, const QString& keySequesnce)
{
  QAction* action = new QAction(name, this);

  if (!keySequesnce.isEmpty())
    action->setShortcut(QKeySequence::fromString(keySequesnce));

  connect(action, SIGNAL(triggered()), this, slot);
  mmenu->addAction(action);
  return action;
}

QAction* Context::addShortcut(const QString& keySequesnce, const char* slot)
{
  QAction* action = new QAction(this);
  action->setShortcut(QKeySequence::fromString(keySequesnce));
  connect(action, SIGNAL(triggered()), this, slot);
  minvisible.append(action);
  return action;
}

void Context::addSeparator()
{
  mmenu->addSeparator();
}

MainWindow* Context::window() const
{
  return AppObject::get()->window();
}

Tab* Context::activeTab() const
{
  return window()->sourcePanel()->activeTab();
}
