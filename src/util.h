#pragma once

class QMetaObject;
class QString;
class QToolButton;
class QWidget;

namespace Util
{

void setMonospaceFont(QWidget* widget);
bool pathEndsWithSepartor(const QString& value);
QString concatPath(const QString path, const QString& file);
const QMetaObject* findMeta(const QString& name);
QToolButton* button(const QString& icon, QWidget* receiver, const char* slot);
QToolButton* button(const QString& icon, const QString& text, QWidget* receiver, const char* slot);

} // namespace Util
