#include "main_ctxpanel.h"

#include <QAction>
#include <QDir>

#include "file_tab.h"
#include "main_panel.h"
#include "main_window.h"

PanelContext::PanelContext(QObject* parent)
  : Context(tr("Panel"), parent)
{
  addAction(tr("New Tab"), SLOT(newTab()), "Ctrl+T");
  mcloseTab = addAction(tr("Close Tab"), SLOT(closeTab()), "Ctrl+W");
  mcloseOtherTab = addAction(tr("Close Other Tabs"), SLOT(closeOtherTabs()), "Ctrl+Shift+W");
  addSeparator();
  addAction(tr("Target = Source"), SLOT(syncTabs()), "Ctrl+U");
  addAction(tr("Taget <-> Source"), SLOT(swapTabs()));
  addAction(tr("Copy Tab to Target"), SLOT(copyTabToTraget()));
  mcollapse = addAction(tr("Collapse Target"), SLOT(collapse()), "Ctrl+0");
  mcollapse->setCheckable(true);
  addSeparator();
  addAction(tr("Go Root"), SLOT(goRoot()), "Ctrl+/");
  addAction(tr("Go Home"), SLOT(goHome()), "Ctrl+`");
}

void PanelContext::updateActions()
{
  MainWindow* win = window();
  bool hasTabs = win->sourcePanel()->canClose();
  mcloseTab->setEnabled(hasTabs);
  mcloseOtherTab->setEnabled(hasTabs);
  mcollapse->setChecked(win->collapsed());
}

void PanelContext::newTab()
{
  window()->newFileTab();
}

void PanelContext::closeTab()
{
  window()->closeTab();
}

void PanelContext::syncTabs()
{
  Tab* source = window()->sourcePanel()->activeTab();
  Tab* target = window()->targetPanel()->activeTab();
  target->setLocation(source->location());
}

void PanelContext::swapTabs()
{
  Tab* source = window()->sourcePanel()->activeTab();
  Tab* target = window()->targetPanel()->activeTab();
  QString srcDir = source->location();
  QString trgDir = target->location();
  source->setLocation(trgDir);
  target->setLocation(srcDir);
}

void PanelContext::closeOtherTabs()
{
  window()->sourcePanel()->closeOther();
}

void PanelContext::copyTabToTraget()
{
  window()->newFileTab(window()->targetPanel(), activeTab()->location());
}

void PanelContext::goRoot()
{
  activeTab()->setLocation(QDir::rootPath());
}

void PanelContext::goHome()
{
  activeTab()->setLocation(QDir::homePath());
}

void PanelContext::collapse()
{
  MainWindow* win = window();
  win->setCollapsed(!win->collapsed());
}
